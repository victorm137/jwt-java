#### links  

intro into jwt  
https://jwt.io/introduction/
https://jwt.io/#debugger


https://ckeditor.com/docs/cs/latest/guides/token-endpoints/tokenendpoint.html#required-payload-properties

### CKSource support
https://ckeditor.com/contact/

https://bitbucket.org/victorm137/jwt-java

https://dashboard.ckeditor.com/organizations/35924/trials/2364/manage


ckeditor5 repo  
https://github.com/ckeditor/ckeditor5

How to Create and verify JWTs in Java  
https://stormpath.com/blog/jwt-java-create-verify   
https://github.com/jwtk/jjwt

thymeleaf example  
https://www.mkyong.com/spring-boot/spring-boot-hello-world-example-thymeleaf/
https://www.thymeleaf.org/doc/tutorials/2.1/thymeleafspring.html

registered claims  
https://tools.ietf.org/html/rfc7519#section-4.1


### logging
https://www.baeldung.com/spring-boot-logging
https://docs.spring.io/spring-boot/docs/current/reference/html/howto-logging.html


==========
Auth0
https://github.com/auth0/java-jwt


### devops

rsync -avz java-web-token-1.0.jar root@5.45.127.213:/home/

nohup java -jar ./java-web-token-1.0.jar &



