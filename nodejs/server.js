const express = require( 'express' );
const jwt = require( 'jsonwebtoken' );

const secretKey = 'xt2wPKNAYuc2u2lNpieCHfWpdpzmtTJbSFUbUaevPsVZKkmmk88Txq9SqnnP';
const environmentId = '6DJZAnF3cQvpQqkmcXnE';

const app = express();

app.get( '/', ( req, res ) => {
    const payload = {
        iss: environmentId,
        user: {
            id: '123',
            email: 'joe.doe@example.com',
            name: 'Joe Doe'
        },
        services: {
            'ckeditor-collaboration': {
                permissions: {
                    '*': 'write'
                }
            }
        }
    };

    const result = jwt.sign( payload, secretKey, { algorithm: 'HS256' } );

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.send( result );
} );

app.listen( 8082, () => console.log( 'Listening on port 8082' ) );
