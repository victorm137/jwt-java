package com.jwt;

import com.auth0.jwt.algorithms.Algorithm;
import com.jwt.cat.CatJWT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * JWT for CKEditor-5 Collaborative Editing.
 *
 * Resulting JWT token can be decoded at https://jwt.io/#debugger
 */
@RestController
public class JWTController {
    private Logger logger = LoggerFactory.getLogger(JWTController.class);

    @RequestMapping("/jwtckeditor")
    public String getJwtForCke5Collaboration(
            @RequestParam(value = "n521") String n521, // required
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "catRight", required = false) String rightToWrite) {

        String jwt = createJwtForCkeCloud(n521, name, email, rightToWrite);
        logger.info("JWT request. " + jwt);

        return jwt;
    }

    private String createJwtForCkeCloud(String n521, String name, String email, String rightToWrite) {

        // TODO: to be moved to config.properties
        // CKEditor-Cloud-Service dashboard (label: Default access credentials)
        String environmentID = "6DJZAnF3cQvpQqkmcXnE";
        String secretKey = "xt2wPKNAYuc2u2lNpieCHfWpdpzmtTJbSFUbUaevPsVZKkmmk88Txq9SqnnP";

        Algorithm algorithm = Algorithm.HMAC256(secretKey);

        String access;
        switch (rightToWrite) {
            case "write":
                access = "write";
                break;
            case "comments":
                access = "comments-only";
                break;
            default:
                access = "read";
        }

        // services
        Map<String, Object> readWrite = new HashMap<>();
        readWrite.put("*", access);
        Map<String, Object> permissions = new HashMap<>();
        permissions.put("permissions", readWrite);
        Map<String, Object> ckeditorCollaboration = new HashMap<>();
        ckeditorCollaboration.put("ckeditor-collaboration", permissions);

        // user
        Map<String, Object> userDetails = new HashMap<>();
        userDetails.put("id", n521);
        if (!name.isEmpty())
            userDetails.put("name", name);
        if (!email.isEmpty())
            userDetails.put("email", email);

        return CatJWT.create()
                .withClaim("services", ckeditorCollaboration)
                .withClaim("user", userDetails)
                .withIssuer(environmentID) // iss
                .withIssuedAt(new Date()) // iat
                .sign(algorithm);
    }

}
