package com.jwt;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
public class EditorController {

    @RequestMapping(value = "/ckeditor", method=RequestMethod.GET)
    public String collaboration(Map<String, Object> model,
                                @RequestParam(value = "n521") String n521,
                                @RequestParam(value = "catDocId") String catDocId,
                                @RequestParam(value = "fullName", required = false) String fullName) {
        model.put("n521", n521);
        model.put("catDocId", catDocId);
        model.put("catName", fullName);

        return "ckeditor";
    }

}
