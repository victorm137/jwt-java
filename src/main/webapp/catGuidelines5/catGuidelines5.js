import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';

import catGuidelines5Icon from '@ckeditor/ckeditor5-ui/theme/icons/dropdown-arrow.svg';
// TODO: import catGuidelines5Icon from './icons/catGuidelinesIcon.svg';


export default class CatGuidelines5 extends Plugin {

    init() {
        const editor = this.editor;

        editor.ui.componentFactory.add('catGuidelines5', locale => {
            const view = new ButtonView(locale);

            view.set({
                label: "Guidelines",
                icon: catGuidelines5Icon,
                tooltip: true
            });

            view.on( 'execute', () => {

                editor.model.change( writer => {
                    const insertPosition = editor.model.document.selection.getFirstPosition();
                    writer.insertText( ' [guidelines] ', { bold: true }, insertPosition );
                } );

            } );

            return view;
        });

    }

    static get pluginName() {
        return 'CatGuidelines5';
    }

}
