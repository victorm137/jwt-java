import ClassicEditor from '@ckeditor/ckeditor5-editor-classic/src/classiceditor';
import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials';
import Paragraph from '@ckeditor/ckeditor5-paragraph/src/paragraph';
import Bold from '@ckeditor/ckeditor5-basic-styles/src/bold';
import Italic from '@ckeditor/ckeditor5-basic-styles/src/italic';
import Underline from '@ckeditor/ckeditor5-basic-styles/src/underline';
import List from '@ckeditor/ckeditor5-list/src/list';

import EasyImageUpload from '@ckeditor/ckeditor5-easy-image/src/easyimage';
import ImageToolbar from '@ckeditor/ckeditor5-image/src/imagetoolbar';
import CollaborativeComments from '@ckeditor/ckeditor5-collaboration/src/collaborativecomments';
import PresenceList from '@ckeditor/ckeditor5-collaboration/src/presencelist';
import CatGuidelines5 from './catGuidelines5/catGuidelines5';


ClassicEditor
    .create(document.querySelector('#editor'), {
        plugins: [Essentials, Paragraph, Bold, Italic, Underline, List, CatGuidelines5, EasyImageUpload, ImageToolbar,
            CollaborativeComments, PresenceList],
        cloudServices: {
            // #2364 Collaboration free trial (ID: 35924) https://dashboard.ckeditor.com/
            // tokenUrl: 'https://35924.cke-cs.com/token/dev/UIE5iWKWYG12s6zIhgffAaEQADSzQItqnsFpZVQMavfd8Xfq8wm5zLj8jPcl',
            tokenUrl: '/jwtckeditor?n521=' + n521 + '&name=' + catName + '&write=true',
            uploadUrl: 'https://35924.cke-cs.com/easyimage/upload/',
            webSocketUrl: '35924.cke-cs.com/ws',
            documentId: catDocId
        },
        toolbar: ['bold', 'italic', 'underline', 'bulletedList', 'catGuidelines5',  'imageUpload', 'comment'],
        presenceList: {
            container: document.querySelector( '.presence-list-container' )
        },
        image: {
            toolbar: [ 'comment' ]
        },
        sidebar: {
            container: document.querySelector( '.sidebar' )
        }
    })
    .then(editor => {
        window.CKEDITOR5 = editor;
        console.log('Editor was initialized', editor);
        console.log(CatGuidelines5.pluginName);

        // sessions
        // const sessionsPlugin = editor.plugins.get( 'Sessions' );
        // console.log(
        //     editor.connectedUsers
        // );
    })
    .catch(error => {
        console.error(error.stack);
    });
